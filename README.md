MagLev - 0.4.2
======

Description
===
MagLev is a intermediary between the CMS system Magento and the HTTP Accelerator Varnish. Since Magento is quite a beast in its hosting requirements, a powerful caching solution like Varnish Is quite desirable.

How it works
===
When a user visits a Varnish powered website the webserver does not process and serve the content to the visitor but
Rather receives sends a cached static copy of the page requested.

The problem with this efficient setup arises when, for example, the user changes availability of a product on sale.
This change would not be visible until the Varnish server updates its cache minutes, even hours later.

MagLev is a module that reacts when the owner of the Magento store changes ANY content on the site. It then connects to the varnish server to force it to update its cache. 

Since not all end user can SSH into a server and check the status of varnish a controllpanel is also available to the user.

Feature list
===

Auto Purges the following items on content/URL change
+   Categories
+   Products
+   CMS pages

Supports the following communication protocols for Varnish interaction
+   HTTP (cURL)
+   TCP (SOCKET)
+   Works with/without secret key authentication

Support large selective operations 
+   Runs a CRON job that removes 100 pages / 5min (configurable)
+   Over the TCP protocol, 42 pages per second are BAN'ed from Varnish cache 
+   Tested with 1000+ products

Basic controllpanel
+   Supports all varnish commands

ToDo:
===
Nothing here =)