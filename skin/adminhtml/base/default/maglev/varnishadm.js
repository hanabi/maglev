var VarnishAdmClass = Class.create({
	initialize: function(wrapper, url) {
		this.ajaxUrl = url;
		this.wrapper = wrapper;
		this.ajaxGetBanner();

		var handle = this;
		
		$('banlistupdatebtn').on('click',function(){handle.ajaxGetBanList();});
		$('getLoadedVclbtn').on('click',function(){handle.ajaxGetVclList();});
		$('ajaxGetstatusbtn').on('click',function(){handle.ajaxGetstatus();});
		$('ajaxStartupbtn').on('click',function(){handle.ajaxStartup();});
		$('ajaxShutdownbtn').on('click',function(){handle.ajaxShutdown();});		
		$('ajaxGetHelpbtn').on('click',function(){handle.ajaxGetHelp();});
				
		/*
			setInterval(function(handle) {
				this.handle.ajaxGetBinaryBoogie();
			}, 2000);
		*/
	},
	ajaxGetBanner: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'banner'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);								
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},
	ajaxGetVclList: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'vcl.list'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);								
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},
	ajaxGetstatus: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'status'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);								
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},
	ajaxStartup: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'start'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);								
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},
	ajaxShutdown: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'stop'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);								
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},				
	ajaxGetHelp: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'help'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},	
	ajaxGetBanList: function() {
		var thisClass = this;
		new Ajax.Request(this.ajaxUrl, {
			parameters: {command:'ban.list'} ,
			onSuccess: function(data) {
				thisClass.ajaxSuccessCallback(data);
			},
			onFailure: function(data){
				thisClass.ajaxSuccessCallback(data);
			}
		});
	},
	ajaxSuccessCallback: function(data) {

		$('ajax-cli').update(data.responseText);
	},
	ajaxFaliureCallback: function(data) {
		alert("Error in communication to the server, please try again or refresh the site.");
	}
});

	