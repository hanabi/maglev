<?php

/**
 * Main Controller class for the Varnish Admin Panel
 */
class Ecom_MagLev_Adminhtml_VarnishadmController extends Mage_Adminhtml_Controller_Action {

	/**
	 * Renders the layout in the tab System -> Tools -> Varnish Admin Panel 
	 * @return void
	 */
	public function indexAction() {
		$tcpModel = Mage::getModel('ecom_maglev/TcpVarnish');

		if ($tcpModel->isSocketAlive()) {
			$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Successfully authenticated with Varnish Socket Interface'));
			$this->loadLayout()->renderLayout();
		} else {
			$this->_getSession()->addError(Mage::helper('adminhtml')->__('Could not connect to Varnish Socket Interface, please check your settings'));
			$this->_redirect('/system_config/edit/section/maglev_options/');
		}
	}

	/**
	 * Passes commands from the Javascript frontend through to the Varnishadm CLI interface.
	 * Uses AJAX POST with the parameter 'command'
	 * @return void
	 */
	public function getWithAjaxAction() {
		$tcpModel = Mage::getModel('ecom_maglev/TcpVarnish');
		switch ($this->getRequest()->getPost('command')) {
			case 'banner':
				echo $tcpModel->getCommandResponse('banner');
				break;
			case 'ban.list':
				echo $tcpModel->getCommandResponse('ban.list');
				break;
			case 'vcl.list':
				echo $tcpModel->getCommandResponse('vcl.list');
				break;
			case 'status':
				echo $tcpModel->getCommandResponse('status');
				break;
			case 'start':
				echo $tcpModel->getCommandResponse('start');
				break;
			case 'stop':
				echo $tcpModel->getCommandResponse('stop');
				break;
			case 'help':
				echo $tcpModel->getCommandResponse('help');
				break;
			default:
				echo 'Unknown command.';
				break;
		}
	}

	/**
	 * Enables internal Magento access controll
	 * Restricts controller routes and menu visibility
	 * @return boolean Right to access
	 */
	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('system/config');
	}

}
