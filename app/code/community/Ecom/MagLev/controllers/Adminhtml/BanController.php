<?php

/**
 * Main Purge Controller class. 
 * Responsible for the routes and logic before the actual purge/ban takes place
 */
class Ecom_MagLev_Adminhtml_BanController extends Mage_Adminhtml_Controller_Action {

	/**
	 * Sends a BAN reqest to flush all available Varnish cache.
	 * Depending on the returning cURL header displays native magento success/notices/warnings. 
	 * @return void
	 */
	public function banAllAction() {

		$response = Mage::helper('maglev')->getBanModel()->banAll();
		;


		$formatedResponse = '(' . $response . ' - ' . Mage::helper('maglev')->getResponseAsString($response) . ')';

		switch ($response) {
			case 200:
				$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('The Varnish cache has been successfully flushed. ' . $formatedResponse));
				break;

			case ($response > 200 && $response < 400):
				$this->_getSession()->addNotice(Mage::helper('adminhtml')->__('Varnish Cache Flush may have failed, please check settings in System > Configuration > Varnish Settings. ' . $formatedResponse));
				break;

			default:
				$this->_getSession()->addError(Mage::helper('adminhtml')->__('Varnish Cache Flush failed. ' . $formatedResponse));
				break;
		}
		$this->_redirect('/cache/index/index');
	}

	/**
	 * Check if variable is Empty or a the wildcard *
	 * @param string
	 * @return boolean
	 */
	private function isEmptyorWildcard($param) {
		if (!$param || strval($param) == '*') {
			return true;
		}
		return false;
	}

	/**
	 * Enables internal Magento access controll
	 * Restricts controller routes and menu visibility
	 * @return boolean Right to access
	 */
	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('system/config');
	}

}
