<?php

class Ecom_MagLev_Model_Resource_Banqueue extends Mage_Core_Model_Resource_Db_Abstract {

	public function _construct() {
		$this->_init('ecom_maglev/banqueue', 'item_id');
	}

}
