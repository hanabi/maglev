<?php

class Ecom_MagLev_Model_Resource_Banqueue_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract {

	public function _construct() {
		$this->_init('ecom_maglev/banqueue');
	}

}
