<?php

/**
 * This class handles Varnish interaction over HTTP
 */
class Ecom_MagLev_Model_HttpVarnish {

	/**
	 * Executes a BAN request to varnish server
	 * @param Mixed $urlContainer Array of strings / string
	 * @param String(Optional) $requestType ''
	 * @return Array $resultArr If param is array, array is returned
	 * @return String $result If param is string, string is returned
	 */
	public function ban($urlContainer, $requestType = 'BAN') {
		if (empty($urlContainer) || ($requestType !== 'BAN' && $requestType !== 'PURGE')) {
			return array(0 => 'false');
		}

		//Create an array to hold all our results upon return
		$resultArr = array();

		//If URL Container is Array loop over all the urls. If not just ban the one url
		if (is_array($urlContainer)) {

			//BAN all the URL's and return the response codes to each url in our result array
			foreach ($urlContainer as $url) {
				$resultArr[$url['item_id']] = $this->sendRequest($requestType, $url['url']);
			}

			//Else just ban the single url and return its responsecode as string
		} else {
			$resultArr[$urlContainer] = $this->sendRequest($requestType, $urlContainer);
		}
		return $resultArr;
	}

	/**
	 * Executes a BAN request to varnish server
	 * @param String $domain Selector
	 * @return String Responsecode
	 * @return Boolean False with invalid parameters
	 */
	public function banAll($domain = '') {
		if ($domain == '') {
			return $this->sendRequest('BAN', '*');
		}
		return $this->sendRequest('BAN', rtrim($domain, '/') . '/*');
	}

	/**
	 * Performs the actuall cURL request
	 * @param string BAN | PURGE
	 * @param string Selector
	 * @return boolean false On fatal error / invalid parameters
	 * @return string Header response
	 */
	private function sendRequest($requestType = '', $url) {
		if (!$requestType === 'BAN' && !$requestType === 'PURGE') {
			return false;
		}

		//Get the varnish server Authority (ex. 127.0.0.1:8080)
		$serverSettingsArr = Mage::getStoreConfig('maglev_options/server_settings');
		$varnishAuthority = $serverSettingsArr['ip'] . ':' . $serverSettingsArr['port'];

		//Prepares and Executes the request
		$curl = curl_init($varnishAuthority . "/" . $url);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $requestType);
		curl_setopt($curl, CURLOPT_HEADER, 1);

		curl_exec($curl);

		$responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		$formatedResponse = $responseCode . '(' . Mage::helper('maglev')->getResponseAsString($responseCode) . ')';

		$this->logBan($requestType . ' ' . $formatedResponse . ' - ' . $varnishAuthority . "/" . $url);

		curl_close($curl);

		return $responseCode;
	}

	/**
	 * Loggs Purge/Ban related data to magento/var/log/purge.log
	 * @param  string URL
	 * @return Void
	 */
	private function logBan($string) {
		if (Mage::getStoreConfig('maglev_options/log_settings/logging_enabled')) {
			Mage::log($string, null, 'http-ban.log');
		}
	}

}
