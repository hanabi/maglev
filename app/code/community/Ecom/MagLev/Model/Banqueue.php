<?php

class Ecom_MagLev_Model_Banqueue extends Mage_Core_Model_Abstract {

	var $coreResource;
	var $readOnlyConnection;
	var $writeOnlyConnection;

	/**
	 * The constructor that initialises the Database connection for the table BanQueue 
	 * @return Void
	 */
	public function _construct() {
		parent::_construct();

		$this->_init('ecom_maglev/banqueue');
		$this->coreResource = Mage::getSingleton('core/resource');
	}

	/**
	 * The Main Banqueue method that is run at intevalls by Cron
	 * Checks if the database is empty, if not Ban a sizable chunk of the URL's 
	 * @return Void
	 */
	public function scheduler() {

		try {

			if (!$this->lowImpactQueueCheck()) {
				return 'No URL\'s in queue';
			}

			$startTime = time();

			$urlQueueArr = $this->getQueue();

			if (empty($urlQueueArr) || !is_array($urlQueueArr)) {
				return 'No URL\'s in queue';
			}

			$banModel = Mage::helper('maglev')->getBanModel();

			$UrlResponseArr = $banModel->ban($urlQueueArr);

			$RowsDeleted = $this->removeBannedRowsByIdArr($UrlResponseArr);
			$errorCount = count($urlQueueArr) - intval($RowsDeleted);
			
			$message = 'Flushed ' . intval($RowsDeleted) . ' URL\'s with ' . $errorCount . ' Errors.' . $this->getQueueLength() . ' URL\'s left in in the queue. Execution time: ';

			return $message . strval(time() - $startTime) . 's';
		} catch (Exception $ex) {
			return false;
		}
	}

	/**
	 * Adds URL's to the Ban Queue
	 * Accepts both String and Array of strings as input
	 * @param Mixed $urlInput String/Array of URL's
	 * @return Boolean Final Addition Status
	 */
	public function add($urlInput) {
		if (empty($urlInput)) {
			return false;
		}
		$query = 'INSERT INTO ' . $this->coreResource->getTableName('ecom_maglev/banqueue') . ' (url) VALUES ';

		try {
			//If we have a queue in Array format, loop over it
			if (is_array($urlInput)) {
				foreach ($urlInput as $url) {
					$query .= '(\'' . $url . '\'),';
				}
				$query = rtrim($query, ',');

				//We have a single URL add it to DB	
			} else {
				$query .= '(\'' . $urlInput . '\')';
			}

			//Return the affected rows of the query
			return $this->writeQuery($query);
		} catch (Exception $ex) {
			//Error, return false;
			return false;
		}
	}

	/**
	 * Internal function to get the a number of URL's from the queue
	 * @return Array URL's in queue
	 */
	private function getQueue() {

		//Loads the specified URL's to ban on each CRON execution. Default 1000
		$banCount = strval(Mage::getStoreConfig('maglev_options/automatic_purge/queuebancount'));

		//Load up the query
		$query = 'SELECT item_id,url FROM ' . $this->coreResource->getTableName('ecom_maglev/banqueue') . '   
					 ORDER BY created_at
				    LIMIT ' . $banCount . ' 
				   ';

		//Return the queue as an Assoc array
		return $this->readQuery($query);
	}

	/**
	 * Internal function to get the number of URL's in queue
	 * @return Array URL's in queue
	 */
	private function getQueueLength() {

		//Load up the query
		$query = 'SELECT COUNT(item_id) FROM ' . $this->coreResource->getTableName('ecom_maglev/banqueue') . ';';

		//Return the length of the query as a string
		return $this->readQuery($query, 'fetchOne');
	}

	/**
	 * A low cost function to check if queue is empty or not when running CRON has run
	 * @return Boolean $queueHasLength Does the queue have length?
	 * @return Boolean False on fatal error  
	 */
	private function lowImpactQueueCheck() {

		//Load up the query
		$query = 'SELECT item_id FROM ' . $this->coreResource->getTableName('ecom_maglev/banqueue') . ' LIMIT 1;';

		//Get the length of the query as a string
		$queueHasLength = $this->readQuery($query, 'fetchOne');

		//Return the result
		return $queueHasLength;
	}

	/**
	 * Internal function to get the number of URL's in queue
	 * @param Array $rowsArrInput	Rows to delete from database. Requires the key "item_id" 
	 * @return Int $result Number of rows affected
	 * @return Boolean False on fatal error 
	 */
	private function removeBannedRowsByIdArr($rowsArrInput = array()) {
		if (empty($rowsArrInput)) {
			return false;
		}

		//Instanciate a empty string for our comma delimited string. 
		$idStr = '';

		//Check if the URL was banned with a response 200 OK if so,
		//Get the item_id of all the Banned URL as a comma delimeted string for input into the sql query
		foreach ($rowsArrInput as $itemId => $responseCode) {
			if($responseCode == 200){
				$idStr .= $itemId . ',';
			}
		}
		
		//Remove the last comma from the string
		$idStrFormated = rtrim($idStr, ',');

		//Load up the query
		$query = 'DELETE FROM ' . $this->coreResource->getTableName('ecom_maglev/banqueue') . ' WHERE item_id IN (' . $idStrFormated . ')';

		//Delete the Succesfully Banned urls form the database table
		$result = $this->writeQuery($query);

		if(empty($result)){
			$result = 0;
		}
		
		//All is well return the number of affected rows
		return $result;
	}

	private function readQuery($query, $fetchMethod = 'fetchAll') {
		if (!is_resource($this->readOnlyConnection)) {
			$this->readOnlyConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
		}
		return $this->query($query, $this->readOnlyConnection, $fetchMethod);
	}

	private function writeQuery($query, $setMethod = 'exec') {
		if (!is_resource($this->writeOnlyConnection)) {
			$this->writeOnlyConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
		}
		return $this->query($query, $this->writeOnlyConnection, $setMethod);
	}

	private function query($query, $connection, $method) {
		if (empty($query)) {
			return false;
		}
		return $connection->$method($query);
	}

}
