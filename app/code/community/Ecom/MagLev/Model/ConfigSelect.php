<?php

/**
 * This model generates custom field values
 */
class Ecom_MagLev_Model_Configselect {

	/**
	 * Returns Custom Field Values for the configuration Tab
	 * @return Array Configuration Values
	 */
	public function toOptionArray() {
		//These values are used for the select fieldtype 'Varnish interaction protocol' in System->Configuration->Varnish Options
		return array(
			 array('value' => 'TCP', 'label' => Mage::helper('maglev')->__('TCP (Socket)')),
			 array('value' => 'HTTP', 'label' => Mage::helper('maglev')->__('HTTP (cURL)')),
		);
	}

}
