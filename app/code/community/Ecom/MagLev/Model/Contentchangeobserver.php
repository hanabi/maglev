<?php

/**
 * Main Observer Class
 * Listens for content change and sends the effected URL's to the BAN queue.
 */
class Ecom_MagLev_Model_ContentChangeObserver {

	/**
	 * Listens to the catalog_product_save_after event.
	 * @param Varien_Event_Observer
	 * @return Boolean Wheither or not the URL's successfully got added to the BanQueue
	 */
	public function autoPurgeProduct(Varien_Event_Observer $observer) {
		if (!Mage::getStoreConfig('maglev_options/automatic_purge/products')) {
			return false;
		}
		//Instantiate a empty array for our queue
		$queueItemsArr = array();

		//Gets the Model in charge of handling the Banqueue
		$queueModel = Mage::getModel('ecom_maglev/banqueue');

		//Gets the handle to the product
		$productDataArr = $observer->getEvent()->getProduct();

		//Gets Product Id
		$productId = $productDataArr->getId();

		//Gets Category Id's
		$procuctCategoryIdArr = $productDataArr->GetCategoryids();

		//Gets Store Id's
		$procuctStoreIdArr = $productDataArr->GetStoreids();

		//Get A Array with all the rewritten URL's
		$queueItemsArr = $this->getRewrittenProductUrlsFromArr($productId, $procuctCategoryIdArr, $procuctStoreIdArr);

		//Return the response of adding items to the queue
		return $queueModel->add(array_unique($queueItemsArr));
	}

	/**
	 * Listens to the catalog_category_save_after event.
	 * @param Varien_Event_Observer
	 * @return Boolean Wheither or not the URL's successfully got added to the BanQueue
	 */
	public function autoPurgeCategories(Varien_Event_Observer $observer) {
		if (!Mage::getStoreConfig('maglev_options/automatic_purge/categories')) {
			return false;
		}
		//Get the BanQueue model loaded into memory once
		$queueModel = Mage::getSingleton('ecom_maglev/banqueue');

		//Instantiate a empty array to hold our queue 
		$queueItemsArr = array();

		//Gets the handle to the category
		$categoryDataArr = $observer->getEvent()->getCategory();

		//Gets Category Id's
		$categoryId = $categoryDataArr->getId();

		//Get all the store ID's the category is visible under 
		$categoryStoreIdArr = $this->removeAdminStore($categoryDataArr->GetStoreids());

		//Get all products that need to be Banned after category change
		$productsCollection = Mage::getModel('catalog/category')->load($categoryId)->getProductCollection();

		//Loop over all the stores the category is visible under add the children products under them
		foreach ($categoryStoreIdArr as $categoryStoreId) {

			//Get the url to the current store
			$storeUrl = Mage::app()->getStore($categoryStoreId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

			//Get the Category URL
			$queueItemsArr[] = rtrim($storeUrl, '/') . '/' . $categoryDataArr->getUrlPath();

			//Get the URL's of all the products under a specific category with all store front variations
			foreach ($productsCollection as $product) {
				///Grab the product url and add it to the BAN queue
				$queueItemsArr[] = $this->getRewrittenProductUrl($product->getId(), $categoryId, $categoryStoreId);
			}
		}
		//Add all unique URL's to the Banqueue
		return $queueModel->add(array_unique($queueItemsArr));
	}

	/**
	 * Listens to the cms_page_prepare_save event.
	 * @param Varien_Event_Observer
	 * @return Boolean Wheither or not the URL's successfully got added to the BanQueue
	 */
	public function autoPurgeCMSPage(Varien_Event_Observer $observer) {
		if (!Mage::getStoreConfig('maglev_options/automatic_purge/cms')) {
			return false;
		}
		//Get the BanQueue model loaded into memory once
		$queueModel = Mage::getSingleton('ecom_maglev/banqueue');

		//Instantiate a empty array to hold our queue 
		$queueItemsArr = array();

		//Get an array version of the Page Object that was just edited with all our data.
		$pageObjectArr = $observer->getEvent()->getPage();

		//Extract the identifier/URL Key
		$pageIdentifier = $pageObjectArr->getIdentifier();

		//Get the Page ID
		$pageId = $pageObjectArr->getPage_id();

		//Get the ID's of the stores this page is visible on
		$storesIdArr = $this->removeAdminStore($pageObjectArr->getStores());

		//If the page is visible in multiple stores then loop over them and BAN them individually
		if (isset($storesIdArr, $pageIdentifier)) {

			//Loop over the stores the page is visible in and BAN them
			foreach ($storesIdArr as $storeId) {

				//Get the store URL from it's ID
				$storeUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

				//Append the identifier(URL Key) to the current store view URl
				$banUrl = rtrim($storeUrl, '/') . '/' . $pageIdentifier;

				//Add the CMS page url to the BAN queue
				$queueItemsArr[] = $banUrl;
			}

			//Multiple stores are not set-up or misconfigured. BAN the CMS page as if there was only one store	
		} else {

			//Get the CMS page URL in the primary store and add it to the queue.
			$queueItemsArr[] = Mage::helper('cms/page')->getPageUrl($pageId);
		}

		//Sometimes the asyncronous 'Save & Continue to Edit' button freezes without a return
		return $queueModel->add(array_unique($queueItemsArr));
	}

	/**
	 * Executes together with the native flush cache storage button in the cache management screen 
	 * BAN's all Varnish cache for ALL domains
	 * @return Int Header response
	 */
	public function banAll() {
		return Mage::helper('maglev')->getBanModel()->banAll();
	}

	/**
	 * Removes the Domain from an URL
	 * @param  String URL
	 * @return String URL Without domain
	 */
	private function removeDomainFromUrl($url) {
		//Gets the domain to be subracted from purge url
		$domain = $_SERVER['HTTP_HOST'];

		//Removes the domain from url and strips away anything preceding it.
		//Then removes any eventual preceding slashes after $domain is removed from the url        
		$url = stristr($url, $domain);
		$url = ltrim(str_replace($domain, "", $url), '/');

		//return our domainless url
		return $url;
	}

	/**
	 * Gets the Absolute Product URL from product handle
	 * @param Varien_Object
	 * @return Boolean false On fatal error / invalid parameters
	 * @return String Absolute URL to product
	 */
	private function getAbsoluteProductUrl($productHandle) {
		if (empty($productHandle)) {
			return false;
		}

		//Gets the absolute URL to the product
		//Uneffected by multiple store fronts
		return $absoluteUrlToProduct = $productHandle->getUrlInStore();
	}

	/**
	 * Gets multiple rewritten product URL's
	 * Takes string and/or Array values
	 * @param String ProductId
	 * @param Mixed CategoryId/Id's
	 * @param Mixed ProductId/Id's
	 * @return Array Product URL's
	 * @return Boolean False on invalid parameters
	 */
	private function getRewrittenProductUrlsFromArr($productId = '', $procuctCategoryIdMixed, $productStoreIdMixed = array()) {
		//If mandatory parameters are missing exit
		if (is_array($productId) || !isset($productStoreIdMixed)) {
			return false;
		}

		if (!is_array($procuctCategoryIdMixed)) {
			$productCategoryIdArr[] = $procuctCategoryIdMixed;
		} else {
			$productCategoryIdArr = $procuctCategoryIdMixed;
		}
		if (!is_array($productStoreIdMixed)) {
			$productStoreIdArr[] = $productStoreIdMixed;
		} else {
			$productStoreIdArr = $productStoreIdMixed;
		}

		//Instantiate a empty array to hold our queue 		
		$queueItemsArr = array();

		//Get all stores the product is visible under.
		//Then remove the admin store front
		$productStoreIdArr = $this->removeAdminStore($productStoreIdArr);

		//Loop over all the stores the product is visible under.
		foreach ($productStoreIdArr as $storeId) {

			//Check if the product has a category.
			//If so loop over all of them
			if (isset($productCategoryIdArr) && count($productCategoryIdArr)) {

				//Looping over the categories
				foreach ($productCategoryIdArr as $categoryId) {
					//Add URL to BAN Queue
					$queueItemsArr[] = $this->getRewrittenProductUrl($productId, $categoryId, $storeId);
				}

				//The product didn't have have a category. Get product URL's without it.
			} else {
				//Grab the product url and add it to the BAN queue
				$queueItemsArr[] = $this->getRewrittenProductUrl($productId, '', $storeId);
			}
		}

		return $queueItemsArr;
	}

	/**
	 * Get a single Rewritten Product URL
	 * @param String ProductId
	 * @param String CategoryId
	 * @param String ProductId
	 * @return String Rewritten Product URL
	 * @return Boolean False on error
	 */
	private function getRewrittenProductUrl($productId, $categoryId, $storeId) {
		//Get the url_rewrite model loaded into memory once
		$rewriteModel = Mage::getSingleton('core/url_rewrite');

		//Set up our id path to run our request on
		$idPath = sprintf('product/%d', $productId);

		//If we have a category id add it to out id string
		if ($categoryId) {
			$idPath = sprintf('%s/%d', $idPath, $categoryId);
		}

		//Set the store URL we want in our request to be rewritten from
		$rewriteModel->setStoreId($storeId);

		//Load our id string into the Rewrite model
		$rewriteModel->loadByIdPath($idPath);

		//Get our Request path from the rewrite model
		$rewrittenRequestPath = $rewriteModel->getRequestPath();

		//Get the base url
		$storeUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

		//Return a fully qualified URL
		return rtrim($storeUrl, '/') . '/' . $rewrittenRequestPath;
	}

	/**
	 * Removes the admin store key from store id's
	 * @param Array Store ID's
	 * @return String Rewritten Product URL
	 * @return Array Store ID's
	 */
	private function removeAdminStore($storeArr = array()) {
		//If param is an array with atlease one value
		//Get the array key which has the store id 0
		if (is_array($storeArr) && count($storeArr) && ($adminStoreKey = array_search('0', $storeArr)) !== false) {

			//Unset the admin store key with the value 0
			unset($storeArr[$adminStoreKey]);
		}
		//Return all our Store ID's without the admin store front key
		return $storeArr;
	}

}
