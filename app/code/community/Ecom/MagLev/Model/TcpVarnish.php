<?php

/**
 * This class handles Varnish interaction over TCP
 */
class Ecom_MagLev_Model_Tcpvarnish {
	/* Creates the variables to hold the connection data loaded upon connection attempt */

	var $socket;
	var $host;
	var $port;
	var $secret;
	var $timeout;

	/**
	 * Executes a BAN request to varnish server
	 * @param Mixed $urlContainer Array of strings / string
	 * @param String(Optional) $requestType ''
	 * @return Array $resultArr If param is array, array is returned
	 * @return String $result If param is string, string is returned
	 */
	public function ban($urlContainer, $requestType = 'BAN') {
		if (empty($urlContainer) || ($requestType !== 'BAN' && $requestType !== 'PURGE')) {
			return array();
		}
		//If the socket isn't alive, reconnect
		if (!is_resource($this->socket)) {
			//If socket wont start terminate BAN method and return empty array()
			if ($this->varnishAdmConnect() === false) {
				return array();
			}
		}

		//Create an empty array() for our response codes
		$resultArr = array();

		if (is_array($urlContainer)) {
			//If we have multiple URL's loop over them and collect the responses in resultArr
			foreach ($urlContainer as $url) {
				$resultArr[$url['item_id']] = $this->command('ban.url ' . $url['url'], $code, 200, true);
			}
			//If we have a single url. Ban it and return response as string
		} else {
			return $resultArr[$urlContainer] = $this->command('ban.url ' . $urlContainer, $code, 200, true);
		}
		return $resultArr;
	}

	/**
	 * Executes a ban all command.
	 * @param String $domain(Optional) The domain to burge all urls under
	 * @return string $response Header Response
	 */
	public function banAll($domain = '') {
		//If the socket isn't alive, reconnect

		if (!is_resource($this->socket)) {

			$this->varnishAdmConnect();
		}

		if (!$domain) {
			$response = $this->command('ban.url .', $code, 200, true);
			$this->logBan($response);
			return $response;
		}
		$response = $this->command('ban.url ' . rtrim($domain, '/') . '.', $code, 200, true);
		$this->logBan($response);
		return $response;
	}

	/**
	 * Executes and returns the output from the specified CLI command.
	 * Uses AJAX POST with the parameter 'command'
	 * @return string Command Output
	 */
	public function getCommandResponse($command) {

		//If the socket isn't alive, reconnect
		if (!is_resource($this->socket)) {
			$this->varnishAdmConnect();
		}

		//Returns the output.
		return $this->command($command, $code);

		//Gracefully close the socket.
		$this->quit();
	}

	/**
	 * Executes and returns the output from the specified CLI command.
	 * Uses AJAX POST with the parameter 'command'
	 * Shows native magento error messages on fail.  
	 * @return boolean
	 */
	private function varnishAdmConnect() {
		$this->loadConfiguration();
		try {
			$this->socket = fsockopen($this->host, $this->port);
		} catch (Exception $Ex) {
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Error socket connection refused. Please check Varnish IP:Port'));
			return false;
		}

		if (!is_resource($this->socket)) {
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('WARNING Could not open a socket connection to Varnish. Please check your settings'));
			return false;
		}

		stream_set_blocking($this->socket, 1);
		stream_set_timeout($this->socket, $this->timeout);

		$banner = $this->read($code);
		if ($code === 107) {
			if (!$this->secret) {
				//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Authentication required. Please enter your secret key'));
				return false;
			}
			try {
				$challenge = substr($banner, 0, 32);
				$response = hash('sha256', $challenge . "\x0A" . $this->secret . "\x0A" . $challenge . "\x0A");
				$banner = $this->command('auth ' . $response, $code, 200);
			} catch (Exception $Ex) {
				//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Authentication failed. Please check your secret key'));
				return false;
			}
		}
		if ($code !== 200) {
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Bad response from varnish. Please check your settings'));
			return false;
		}
		return true;
	}

	/**
	 * Executes a read operation on the socket.
	 * Shows native magento error messages on fail. 
	 * @return string response	 
	 * @return boolean
	 */
	private function read(&$code) {
		$code = null;
		if (!is_resource($this->socket)) {
			return false;
		}

		while (!feof($this->socket)) {
			$response = fgets($this->socket, 1024);
			if (!$response) {
				$meta = stream_get_meta_data($this->socket);
				if ($meta['timed_out']) {
					//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Socket Read timeout'));
					return false;
				}
			}
			if (preg_match('/^(\d{3}) (\d+)/', $response, $r)) {
				$code = (int) $r[1];
				$len = (int) $r[2];
				break;
			}
		}
		if (is_null($code)) {
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Failed to get header response. Does varnish listen on this port?'));
			return false;
		}
		$response = '';
		while (!feof($this->socket) && strlen($response) < $len) {
			$response .= fgets($this->socket, 1024);
		}
		return $response;
	}

	/**
	 * Executes a write operation on the socket.
	 * Shows native magento error messages on fail.
	 * @return boolean
	 */
	private function write($data) {
		try {
			$bytes = fputs($this->socket, $data);
		} catch (Exception $Ex) {
			return false;
		}

		if ($bytes !== strlen($data)) {
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Write comunication to varnish failed'));
			return false;
		}
		return true;
	}

	/**
	 * Executes a command towards the CLI interface
	 * Shows native magento error messages on fail.
	 * @return string Response	 
	 * @return boolean
	 */
	private function command($cmd, &$code, $ok = 200, $returnResponsecode = false) {
		$cmd and $this->write($cmd);
		$this->write("\n");
		$response = $this->read($code);
		if ($code !== $ok) {
			$response = implode("\n > ", explode("\n", trim($response)));
			//$this->_getSession()->addError(Mage::helper('adminhtml')->__('Write comunication to varnish failed'));
			if ($returnResponsecode) {
				return 400;
			}
			return false;
		}
		if ($returnResponsecode) {
			return $code;
		}
		return $response;
	}

	/**
	 * Loads the connection data into the class on connection attempt
	 * @return void
	 */
	private function loadConfiguration() {

		$this->host = Mage::getStoreConfig('maglev_options/server_settings/ip');
		$this->port = Mage::getStoreConfig('maglev_options/server_settings/port');
		$this->secret = Mage::getStoreConfig('maglev_options/server_settings/secret');
		$this->timeout = Mage::getStoreConfig('maglev_options/server_settings/timeout');
	}

	/**
	 * Checks if socket is live	 
	 * @return boolean
	 */
	public function isSocketAlive() {
		return $this->varnishAdmConnect();
	}

	/**
	 * Graceful shutdown
	 * Calles close() for a forced shutdown upon fail
	 * @return void
	 */
	private function quit() {
		try {
			$this->command('quit', $code, 500);
		} catch (Exception $Ex) {
			// silent fail call close()
		}
		$this->close();
	}

	/**
	 * Forces the socket to shutdown. 
	 * Is called as a failsafe from quit()
	 * @return void
	 */
	private function close() {
		is_resource($this->socket) and fclose($this->socket);
		$this->socket = null;
	}

	/**
	 * Loggs Purge/Ban related data to magento/var/log/purge.log
	 * @param  string URL
	 */
	private function logBan($string) {
		if (Mage::getStoreConfig('maglev_options/log_settings/logging_enabled')) {
			Mage::log($string, null, 'tcp-ban.log');
		}
	}

}
