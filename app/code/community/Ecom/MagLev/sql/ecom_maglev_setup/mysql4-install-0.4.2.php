<?php

/*
 * This Creates our default database for the 0.4.2 version of MagLev
 */

$installer = $this;

$installer->startSetup();

$installer->run("
						DROP TABLE IF EXISTS {$installer->getTable('ecom_maglev/banqueue')};
						
						CREATE TABLE IF NOT EXISTS {$installer->getTable('ecom_maglev/banqueue')} (
							
							`item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Item ID',
							`url` varchar(2083) DEFAULT NULL COMMENT 'URL Path to BAN',
							`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Creation Time',
							
							PRIMARY KEY (`item_id`),
							UNIQUE KEY `item_id` (`item_id`)
						)
						
						ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Varnish BAN Queue' AUTO_INCREMENT=8 ;						
						SET FOREIGN_KEY_CHECKS=1;"
);

$installer->endSetup();
