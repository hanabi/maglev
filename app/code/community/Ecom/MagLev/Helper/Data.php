<?php

/*
 * This Class extends the required dependencies for our helper functions
 */

class Ecom_MagLev_Helper_Data extends Mage_Core_Helper_Abstract {

	/**
	 * Returns a admin URL to specified controller/method with nounce key included *
	 * @param string controller name without slashes
	 * @param string method name without slashes 
	 * @return string URL to 'specified resource'
	 * @return bool false on failure
	 */
	public function getAdminUrl($controllerName = '', $methodName = '') {
		if(!$controllerName || !$methodName){
			return false;
		}
		$cleanController = $this->stripNonAlpha($controllerName);
		$cleanMethod = $this->stripNonAlpha($methodName);
		
		
		return Mage::helper("adminhtml")->getUrl('adminhtml/' . $cleanController . '/' . $cleanMethod);
	}	

	public function getBanModel(){
		if(!$protocol = Mage::getStoreConfig('maglev_options/server_settings/protocol')){
			$protocol = 'TCP';
		}
		
		//Convert protocall to lowercase then capitalise first char to match model name
		$modelName = ucfirst(strtolower($protocol)) . 'Varnish';
		
		return Mage::getModel('ecom_maglev/' . $modelName);			
	}	
	
	/**
	 * Remove all characters except letters.
	 *
	 * @param string $string
	 * @return string
	 */
	public function stripNonAlpha($string) {
		return preg_replace("/[^a-z]/i", "", $string);
	}
	
	/**
	 * Transforms string based header codes and returns 'human' response strings
	 * @param int
	 * @return string Ex. 200 becomes 'Request OK'
	 */
	public function getResponseAsString($responseCode) {
		switch (strval($responseCode)) {
			case '100':
				return 'Continue';
			case '101':
				return 'Switching Protocols';
			case '200':
				return 'Request OK';
			case '201':
				return 'Created';
			case '202':
				return 'Accepted';
			case '203':
				return 'Non-Authoritative Information';
			case '204':
				return 'No Content';
			case '205':
				return 'Reset Content';
			case '206':
				return 'Partial Content';
			case '300':
				return 'Multiple Choices';
			case '301':
				return 'Moved Permanently';
			case '302':
				return 'Found';
			case '303':
				return 'See Other';
			case '304':
				return 'Not Modified';
			case '305':
				return 'Use Proxy';
			case '306':
				return '(Unused)';
			case '307':
				return 'Temporary Redirect';
			case '400':
				return 'Bad Request';
			case '401':
				return 'Unauthorized';
			case '402':
				return 'Payment Required';
			case '403':
				return 'Forbidden';
			case '404':
				return 'Not Found';
			case '405':
				return 'Method Not Allowed';
			case '406':
				return 'Not Acceptable';
			case '407':
				return 'Proxy Authentication Required';
			case '408':
				return 'Request Timeout';
			case '409':
				return 'Conflict';
			case '410':
				return 'Gone';
			case '411':
				return 'Length Required';
			case '412':
				return 'Precondition Failed';
			case '413':
				return 'Request Entity Too Large';
			case '414':
				return 'Request-URI Too Long';
			case '415':
				return 'Unsupported Media Type';
			case '416':
				return 'Requested Range Not Satisfiable';
			case '417':
				return 'Expectation Failed';
			case '500':
				return 'Internal Server Error';
			case '501':
				return 'Not Implemented';
			case '502':
				return 'Bad Gateway';
			case '503':
				return 'Service Unavailable';
			case '504':
				return 'Gateway Timeout';
			case '505':
				return 'HTTP Version Not Supported';
			default:
				return 'Unknown header';
		}
	}

}
